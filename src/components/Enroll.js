import {
  Grid,
  Typography,
  Button,
  Card,
  CardHeader,
  CardContent,
  CardActions,
  CardMedia,
  Tabs,
  Tab
} from "@material-ui/core";
import React from "react";
import "./styles.css";
import apiInstance from "../api";
import { yyyymmdd } from "./";
import { withRouter, Route, Link, Switch } from "react-router-dom";
import { Cancel } from "@material-ui/icons";

const enrollTypes = ["All", "Challenge", "Programme", "Event", "Job"];
function EnrollCard(props) {
  const data = props.data,
    apply = !props.applied,
    callback = props.callback;
  function action() {
    (async () => {
      let response = await apiInstance.requestWithAuth(
        "private/dynamic_challenge_apply",
        { id: data.challenge_id, operation: apply ? "insert" : "delete" }
      );
      await callback();
    })();
  }
  return (
    <Card style={{ padding: "10px" }}>
      <CardHeader title={data.name} subheader={enrollTypes[data.category]} />
      {data.banner_image ? (
        <CardMedia
          style={{ paddingTop: "56.25%" }}
          image={data.banner_image}
          title={data.name}
        />
      ) : null}
      <CardContent title={data.name}>
        <Typography variant="body1">
          ENDS: {yyyymmdd(new Date(data.end_date_time))}
        </Typography>
        <Typography variant="body2">
          {data.desc}
        </Typography>
      </CardContent>
      <CardActions>
        {!apply ? (
          <Typography
            variant="body1"
            style={{ height: "100%", textAlign: "right", padding: "5px" }}
          >
            Applied
          </Typography>
        ) : null}
        <Button
          onClick={e => {
            e.stopPropagation();
            action();
          }}
          color={apply ? "primary" : "default"}
        >
          {apply ? "APPLY" : <Cancel />}
        </Button>
        {data.url ? (
          <Button href={data.url} target="_blank">
            LEARN MORE
          </Button>
        ) : null}
      </CardActions>
    </Card>
  );
}

class Enroll extends React.Component {
  category;
  constructor(props) {
    super(props);
    this.category = props.category ? props.category : 0;
    this.state = {
      challengesAvaliable: [],
      challengesApplied: []
    };
    this.setState = this.setState.bind(this);
    this.handleListClick = this.handleListClick.bind(this);
  }
  componentDidMount() {
    this.handleListClick();
  }
  handleListClick() {
    (async () => {
      let response = await apiInstance.requestWithAuth(
        "private/challenge_available",
        { category: this.category }
      );
      if (response && !response.status && !response.error_msg) {
        this.setState({ challengesAvailable: response });
      } else this.setState({ challengesAvailable: [] });
    })();
    (async () => {
      let response = await apiInstance.requestWithAuth(
        "private/challenge_applied",
        { category: this.category }
      );
      if (response && !response.status && !response.error_msg) {
        this.setState({ challengesApplied: response });
      } else this.setState({ challengesApplied: [] });
    })();
  }
  render() {
    return (
      <Grid container spacing={2}>
        {this.state.challengesApplied && this.state.challengesApplied.length > 0
          ? this.state.challengesApplied.map(challengeApplied => {
            return (
              <Grid item xs={12} md={6} lg={4}>
                <EnrollCard
                  callback={this.handleListClick}
                  data={challengeApplied}
                  applied
                />
              </Grid>
            );
          })
          : null}
        {this.state.challengesAvailable &&
          this.state.challengesAvailable.length > 0
          ? this.state.challengesAvailable.map(challengeAvailable => {
            return (
              <Grid item xs={12} md={6} lg={4}>
                <EnrollCard
                  callback={this.handleListClick}
                  data={challengeAvailable}
                />
              </Grid>
            );
          })
          : null}
      </Grid>
    );
  }
}

function Programmes() {
  return <Enroll category={2} />;
}

function Events() {
  return <Enroll category={3} />;
}

function Challenges() {
  return <Enroll category={1} />;
}

const Enrolls = withRouter(props => {
  const allTabs = [
    {
      path: "/enroll",
      name: "All",
      exact: true,
      view: true,
      component: <Enroll />
    },
    {
      path: "/enroll/events",
      name: "Events",
      view: true,
      component: <Events />
    },
    {
      path: "/enroll/programmes",
      name: "Programmes",
      view: true,
      component: <Programmes />
    },
    {
      path: "/enroll/challenges",
      name: "Challenges",
      view: true,
      component: <Challenges />
    }
  ];

  return (
    <div>
      <Tabs variant="scrollable" value={props.location.pathname}>
        {allTabs.map(value => (
          <Tab
            value={value.path}
            component={Link}
            to={value.path}
            label={value.name}
          />
        ))}
      </Tabs>
      <Switch location={props.location}>
        {allTabs.map(value =>
          value.component ? (
            <Route path={value.path} exact={value.exact}>
              {value.component}
            </Route>
          ) : null
        )}
      </Switch>
    </div>
  );
});
export { EnrollCard, Enroll, Programmes, Events, Challenges, Enrolls };

const sample = {
  id: 2,
  user_id: 1,
  name: "Bengal Coding Challenge",
  desc: "Organised by Government of West Bengal",
  banner_image: null,
  status: true,
  start_date_time: "2020-06-28T18:30:00.000Z",
  end_date_time: "2020-06-30T18:30:00.000Z",
  create_date_time: null,
  modified_date_time: null,
  category: 1,
  url: null
};
