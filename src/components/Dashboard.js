import React, { useState } from "react";
import {
  Grid,
  Button,
  Tabs,
  Tab,
  Card,
  CardActionArea,
  CardMedia,
  CardContent,
  CardActions,
  Typography,
  Modal,
  Paper,
  TextField
} from "@material-ui/core";
import { Link, Switch, Route } from "react-router-dom";
import Carousel from "react-material-ui-carousel";
import { Home } from "@material-ui/icons";
import apiInstance from "../api";
const ComponentSwitch = props => {
  return (
    <Switch location={props.location}>
      {props.home ? (
        <Route path="/" exact>
          <Dashboard />
        </Route>
      ) : null}
      {props.tabs.map(value =>
        value.component ? (
          <Route path={value.path}>{value.component}</Route>
        ) : null
      )}
    </Switch>
  );
};

const FileUpload = props => {
  const [open, setOpen] = useState(false);
  const [url, setUrl] = useState(null);

  const onChangeHandler = async event => {

    const data = new FormData();
    data.append("file", event.target.files[0]);
    let response = await apiInstance.requestWithAuth(
      "private/upload",
      data, true
    );
    if (response && !response.error) {
      if (props.callback) await setUrl(response.url);
      else await setUrl(null);
    }
  };
  return (
    <React.Fragment>
      <Button
        fullWidth={props.fullWidth}
        startIcon={props.startIcon}
        onClick={() => {
          setOpen(true);
        }}
      >
        {props.label}
      </Button>
      <Modal open={open}>
        <Paper
          style={{
            padding: "5px",
            position: "relative",
            top: "50%",
            left: "50%",
            transform: "translate(-50%,-50%)",
            maxWidth: "400px"
          }}
        >
          <Typography variant="h4">{props.label}</Typography>
          <input type="file" name="file" onChange={onChangeHandler} />
          <Typography variant="body1">OR</Typography>
          <TextField
            label="Enter URL"
            value={url}
            onChange={event => {
              setUrl(event.target.value);
            }}
            fullWidth
          />
          {url ?
            <Button
              onClick={() => {
                props.callback(url);
                setUrl(null);
                setOpen(false);
              }}
              color="primary"
              fullWidth
            >
              Continue
          </Button> : null}
          <Button
            onClick={() => {
              setOpen(false);
              setUrl(null);
            }}
            fullWidth
          >
            Cancel
          </Button>
        </Paper>
      </Modal>
    </React.Fragment>
  );
};

const Banners = props => {
  const banners = [
    {
      label: "Turing Challenge",
      description: "Turing Challenge - The largest hackathon of West Bengal.",
      image:
        "https://s3.amazonaws.com/media.skillcrush.com/skillcrush/wp-content/uploads/2017/04/Blog_coding-game.jpg.webp",
      action: "#",
      action2: "/enroll/events",
      action2Label: "See all events"
    },
    {
      label: "Web Developer",
      description: "Magnox Technologies Pvt. Ltd., Kolkata, WB",
      image: "http://techmagnox.com/assets/images/logo.png",
      action: "#",
      action2: "/jobs",
      action2Label: "See all jobs"
    }
  ];
  return (
    <Carousel>
      {banners.map(value => (
        <Card>
          <CardActionArea href={value.action} target="_blank">
            <CardMedia
              image={value.image}
              style={{ height: "400px" }}
              title={value.label}
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="h2">
                {value.label}
              </Typography>
              <Typography variant="body2" color="textSecondary" component="p">
                {value.description}
              </Typography>
            </CardContent>
          </CardActionArea>
          <CardActions>
            <Button
              fullWidth
              component={Link}
              to={value.action2}
              color="primary"
            >
              {value.action2Label}
            </Button>
          </CardActions>
        </Card>
      ))}
    </Carousel>
  );
};

const NavTabs = props => {
  let mainTab = "/" + props.value.split("/")[1];
  return (
    <Tabs
      value={mainTab}
      indicatorColor="secondary"
      variant="scrollable"
      orientation={props.vertical ? "vertical" : "horizontal"}
    >
      {props.vertical ? (
        <Tab
          value="/"
          icon={<Home />}
          component={Link}
          to="/"
          onClick={props.callback}
        />
      ) : null}
      {props.tabs.map(value =>
        value.view ? (
          value.icon ? (
            <Tab
              value={value.path}
              component={Link}
              to={value.path}
              icon={value.icon}
              onClick={props.callback}
            />
          ) : (
              <Tab
                value={value.path}
                component={Link}
                to={value.path}
                label={value.name}
                onClick={props.callback}
              />
            )
        ) : null
      )}
    </Tabs>
  );
};

const Dashboard = props => {
  return (
    <Grid container spacing={1}>
      <Grid item xs={6} md={4} lg={3}>
        <Button
          component={Link}
          fullWidth
          variant="outlined"
          to="/posts_self"
          color="primary"
        >
          My Posts
        </Button>
      </Grid>
      <Grid item xs={6} md={4} lg={3}>
        <Button
          component={Link}
          fullWidth
          variant="outlined"
          to="/messages"
          color="primary"
        >
          Messages
        </Button>
      </Grid>
      <Grid item xs={6} md={4} lg={3}>
        <Button
          fullWidth
          component={Link}
          variant="outlined"
          to="/forum"
          color="primary"
        >
          Forum
        </Button>
      </Grid>
      <Grid item xs={6} md={4} lg={3}>
        <Button
          component={Link}
          fullWidth
          variant="outlined"
          to="/connects"
          color="primary"
        >
          Connects
        </Button>
      </Grid>
      <Grid item xs={6} md={4} lg={3}>
        <Button
          component={Link}
          fullWidth
          variant="outlined"
          to="/video_cover"
          color="primary"
        >
          Video Cover
        </Button>
      </Grid>
      <Grid item xs={6} md={4} lg={3}>
        <Button
          component={Link}
          fullWidth
          variant="outlined"
          to="/import_resume"
          color="primary"
        >
          Import Resume
        </Button>
      </Grid>
      <Grid item xs={6} md={4} lg={3}>
        <Button
          fullWidth
          variant="outlined"
          href="#"
          target="_blank"
          color="primary"
        >
          Conclave
        </Button>
      </Grid>
      <Grid item xs={6} md={4} lg={3}>
        <Button
          fullWidth
          variant="outlined"
          href="#"
          target="_blank"
          color="primary"
        >
          GraphWeb
        </Button>
      </Grid>
    </Grid>
  );
};

export { Dashboard, ComponentSwitch, NavTabs, Banners, FileUpload };
