import { Edit, CloudDownload, ArrowForward, List, Delete, Facebook, YouTube, Web, Cancel, Done, CloudUpload } from "@material-ui/icons";
import {
  Paper,
  TextField,
  Grid,
  Avatar,
  Snackbar,
  Button,
  IconButton,
  Typography,
  Select,
  InputLabel,
  FormControl,
  MenuItem,
  Chip,
  Divider,
  Tabs,
  Tab,
  CardHeader,
  Card,
  CardContent,
  CardActions
} from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import { Academic, Professional } from "./";
import React, { useState } from "react";
import { withRouter, Route, Switch, Link } from "react-router-dom";
import apiInstance from "../api";
import { Skill } from "./Skill";
import ReactPlayer from "react-player";
import { FileUpload } from "./Dashboard";
class ProfileCompact extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      userDetails: {},
      professional: {},
      skills: []
    };
    this.setState = this.setState.bind(this);
  }
  componentDidMount() {
    (async () => {
      let result = await apiInstance.refetchUserData();
      let response = await apiInstance.requestWithAuth("private/professional", {
        id: 0
      });
      let response2 = await apiInstance.requestWithAuth(
        "private/user_skill",
        {}
      );
      if (response && response2 && result && !result.error_msg) {
        this.setState({
          professional: response[0],
          userDetails: result,
          skills: response2
        });
        //TODO: add more
      } else {
        console.dir({ response, response2, result });
      }
    })();
  }
  render() {
    return (
      <Paper style={{ padding: "5px" }} elevation={5}>
        <Grid container direction="column" alignItems="stretch" spacing={1}>
          <Grid item container justify="flex-end">
            <IconButton component={Link} to="/profile">
              <Edit />
            </IconButton>
          </Grid>
          <Grid item container justify="center">
            <Avatar
              src={this.state.userDetails.photo_sm}
              style={{ width: "128px", height: "128px" }}
            ></Avatar>
          </Grid>
          <Grid item>
            <Typography variant="h4">
              {this.state.userDetails.first_name}{" "}
              {this.state.userDetails.last_name}
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant="body1">
              {this.state.userDetails.resume_heading}
            </Typography>
            <Typography variant="body2">
              {this.state.professional.company}
            </Typography>
          </Grid>
          <Grid item container justify="space-evenly">
            <Paper style={{ padding: "5px" }} variant="outlined">
              <Typography variant="button" display="inline">
                Resume</Typography>
              <FileUpload startIcon={<CloudUpload />} callback={(url) => { }} />
              <IconButton
                href={this.state.userDetails.resume_url} target="_blank"
              >
                <CloudDownload />
              </IconButton>
            </Paper>
            <Button
              color="primary"
              size="small"
              startIcon={<List />}
              component={Link}
              to="/portfolio"
            >
              Portfolio
            </Button>
          </Grid>
          <Grid item>
            <Divider />
          </Grid>
          <Grid item container>
            <Grid item xs={10} container justify="center">
              <Typography variant="h5">My Skills</Typography>
            </Grid>
            <Grid item xs={2} container justify="flex-end">
              <IconButton component={Link} to="/profile/skills">
                <Edit />
              </IconButton>
            </Grid>
          </Grid>
          <Grid item container spacing={1} justify="space-evenly">
            {this.state.skills.map(skill => (
              <Grid item>
                <Chip color="secondary" label={skill.name} />
              </Grid>
            ))}
          </Grid>
          <Grid item container justify="flex-end">
            <IconButton component={Link} to="/view_profile">
              <ArrowForward />
            </IconButton>
          </Grid>
        </Grid>
      </Paper>
    );
  }
}

function yyyymmdd(date) {
  var mm = date.getMonth() + 1; // getMonth() is zero-based
  var dd = date.getDate();

  return [
    date.getFullYear(),
    (mm > 9 ? "" : "0") + mm,
    (dd > 9 ? "" : "0") + dd
  ].join("-");
}

class CitySelect extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      city: props.city,
      country: 0,
      state: 0,
      cities: [],
      countries: [],
      states: []
    };
  }
  async fetchCityDetails() {
    let response = await apiInstance.request("public/city_details", {
      city_id: this.state.city
    });
    if (response && !response.status)
      this.setState({
        city: response.city_id,
        state: response.state_id,
        country: response.country_id
      });
  }
  async fetchCities() {
    let response = await apiInstance.request("public/city", {
      state_id: this.state.state
    });
    if (response && !response.status) this.setState({ cities: response });
    else this.setState({ cities: [] });
    this.forceUpdate();
  }
  async fetchStates() {
    let response = await apiInstance.request("public/state", {
      country_id: this.state.country
    });
    if (response && !response.status) this.setState({ states: response });
    else this.setState({ states: [] });
    this.forceUpdate();
  }
  async fetchCountries() {
    let response = await apiInstance.request("public/country", {});
    if (response && !response.status) {
      this.setState({ countries: response });
    } else this.setState({ countries: [] });
  }
  async updateCity() {
    apiInstance.requestWithAuth("private/update_user_detail", {
      field_name: "city_id",
      field_data: this.state.city
    });
  }
  componentDidMount() {
    (async () => {
      if (this.state.city) {
        await this.fetchCityDetails();
        await this.fetchCountries();
        await this.fetchStates();
        await this.fetchCities();
      } else {
        await this.fetchCountries();
        await this.setState({ country: this.state.countries[0].id });
        await this.fetchStates();
        await this.setState({ state: this.state.states[0].id });
        await this.fetchCities();
      }
    })();
  }
  render() {
    return (
      <Grid container spacing={1}>
        <Grid item xs={12} md={4}>
          <FormControl size="small" fullWidth>
            <InputLabel>Country</InputLabel>
            <Select
              value={this.state.country}
              onChange={async event => {
                await this.setState({ country: event.target.value });
                await this.fetchStates();
              }}
            >
              {this.state.countries.map(country => {
                return <MenuItem value={country.id}>{country.name}</MenuItem>;
              })}
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={12} md={4}>
          <FormControl size="small" fullWidth>
            <InputLabel>State</InputLabel>
            <Select
              value={this.state.state}
              onChange={async event => {
                await this.setState({ state: event.target.value });
                await this.fetchCities();
              }}
            >
              {this.state.states.map(state => {
                return <MenuItem value={state.id}>{state.name}</MenuItem>;
              })}
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={12} md={4}>
          <FormControl size="small" fullWidth>
            <InputLabel>City</InputLabel>
            <Select
              value={this.state.city}
              onChange={async event => {
                await this.setState({ city: event.target.value });
                await this.updateCity();
              }}
            >
              {this.state.cities.map(city => {
                return <MenuItem value={city.id}>{city.name}</MenuItem>;
              })}
            </Select>
          </FormControl>
        </Grid>
      </Grid>
    );
  }
}

class MothertongueSelect extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mothertongue: props.mothertongue,
      mothertongues: []
    };
  }
  async fetchMothertongues() {
    let response = await apiInstance.request("public/mothertongue", {});
    if (response && !response.status && !response.error_msg) {
      this.setState({ mothertongues: response });
    } else this.setState({ mothertongues: [] });
  }
  async updateMothertongue() {
    apiInstance.requestWithAuth("private/update_user_detail", {
      field_name: "mothertongue_id",
      field_data: this.state.mothertongue
    });
  }
  componentDidMount() {
    (async () => {
      await this.fetchMothertongues();
      if (!this.state.mothertongue) {
        await this.fetchMothertongues();
        await this.setState({ mothertongue: this.state.mothertongues[0].id });
      }
    })();
  }
  render() {
    return (
      <FormControl size="small" fullWidth>
        <InputLabel>Mothertongue</InputLabel>
        <Select
          value={this.state.mothertongue}
          onChange={async event => {
            await this.setState({ mothertongue: event.target.value });
            await this.updateMothertongue();
          }}
        >
          {this.state.mothertongues.map(mothertongue => {
            return (
              <MenuItem value={mothertongue.id}>{mothertongue.lang}</MenuItem>
            );
          })}
        </Select>
      </FormControl>
    );
  }
}

let ProfileComponent = withRouter(props => {
  const userDetails = JSON.parse(props.data);
  const [error, setError] = useState("");
  const [showError, showErrorSet] = useState(false);
  const [firstName, setFirstName] = useState(userDetails.first_name);
  const [lastName, setLastName] = useState(userDetails.last_name);
  const [fathersName, setFathersName] = useState(userDetails.fathers_name);
  const [fathersOccupation, setFathersOccupation] = useState(
    userDetails.fathers_occupation
  );
  const [mothersName, setMothersName] = useState(userDetails.mothers_name);
  const [mothersOccupation, setMothersOccupation] = useState(
    userDetails.mothers_occupation
  );
  const [email, setEmail] = useState(userDetails.email);
  const [phone, setPhone] = useState(userDetails.phone);
  const [altEmail, setAltEmail] = useState(userDetails.alt_email);
  const [altPhone, setAltPhone] = useState(userDetails.alt_phone);
  const [gender, setGender] = useState(userDetails.gender);
  const [address, setAddress] = useState(userDetails.address);
  const [maritalstatus, setMaritalstatus] = useState(userDetails.maritalstatus);
  const [bloodgroup, setBloodgroup] = useState(userDetails.bloodgroup);
  const [pin, setPin] = useState(userDetails.pin);
  const [caste, setCaste] = useState(userDetails.caste);
  const [physicalChallenge, setPhysicalChallenge] = useState(
    userDetails.physical_challenge
  );
  const [percentagePh, setPercentagePh] = useState(userDetails.percentage_ph);
  const [adhaarNo, setAdhaarNo] = useState(userDetails.aadhar_no);
  const [passportNo, setPassportNo] = useState(userDetails.passport_no);
  const [website, setWebsite] = useState(userDetails.website);
  const [facebookLink, setFacebookLink] = useState(userDetails.facebook_link);
  const [linkedinLink, setLinkedInLink] = useState(userDetails.linkedin_link);
  const [googleLink, setGoogleLink] = useState(userDetails.google_link);
  const [aboutMe, setAboutMe] = useState(userDetails.about_me);
  const [videoUrl, setVideoUrl] = useState(userDetails.video_url);
  const [expectedCTC, setExpectedCTC] = useState(userDetails.expected_ctc);
  const [videoEdit, setVideoEdit] = useState(false);
  const [resumeHeading, setResumeHeading] = useState(
    userDetails.resume_heading
  );
  const [dateOfBirth, setDateOfBirth] = useState(userDetails.dateofbirth);

  function updateDetailsField(event, data = null) {
    apiInstance.requestWithAuth("private/update_user_detail", {
      field_name: data ? data.name : event.target.name,
      field_data: data ? data.value : event.target.value
    });
  }
  return (
    <Grid container spacing={1} alignItems="stretch" alignContent="stretch">
      <Grid item xs={12} md={6}>
        <Paper elevation={5} style={{ marginTop: "10px" }}>
          <form>
            <Grid
              container
              style={{ padding: "10px" }}
              justify="center"
              spacing={2}
            >
              <Grid item xs={12} md={12} style={{ height: "25px" }}></Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  fullWidth
                  size="small"
                  label="First Name"
                  onBlur={updateDetailsField}
                  value={firstName}
                  onChange={event => {
                    setFirstName(event.target.value);
                  }}
                  type="name"
                  name="first_name"
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  fullWidth
                  size="small"
                  label="Last Name"
                  value={lastName}
                  onBlur={updateDetailsField}
                  onChange={event => {
                    setLastName(event.target.value);
                  }}
                  type="name"
                  name="last_name"
                />
              </Grid>
              <Grid item xs={12} md={12}>
                <TextField
                  fullWidth
                  size="small"
                  label="Address"
                  value={address}
                  onBlur={updateDetailsField}
                  onChange={event => {
                    setAddress(event.target.value);
                  }}
                  type="address"
                  name="address"
                />
              </Grid>
              <Grid item xs={12} md={12}>
                <CitySelect city={userDetails.city_id} />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  fullWidth
                  size="small"
                  label="PIN"
                  value={pin}
                  onBlur={updateDetailsField}
                  onChange={event => {
                    setPin(event.target.value);
                  }}
                  type="number"
                  name="pin"
                />
              </Grid>
              <Grid item xs={12} md={6}>
                <TextField
                  fullWidth
                  size="small"
                  label="Phone"
                  value={phone}
                  onBlur={updateDetailsField}
                  onChange={event => {
                    setPhone(event.target.value);
                  }}
                  type="phone"
                  name="phone"
                />
              </Grid>
            </Grid>
          </form>
        </Paper>
      </Grid>
      <Grid item xs={12} md={6}>
        <Card>
          <CardHeader title="Video Cover"></CardHeader>{apiInstance.getUserData().video_url && !videoEdit ?
            <React.Fragment>
              <CardContent>
                <ReactPlayer
                  width="100%"
                  height="256px"
                  url={videoUrl}
                /></CardContent>
              <CardActions style={{ display: "flex", justifyContent: "center" }}>
                <IconButton onClick={() => { setVideoEdit(true) }}><Edit /></IconButton>
                <IconButton onClick={() => { updateDetailsField(null, { name: "video_url", value: "" }); setVideoUrl(""); apiInstance.refetchUserData(); }}><Delete /></IconButton>
              </CardActions>
            </React.Fragment> : <React.Fragment>
              <CardContent>
                <Facebook /><YouTube /><Web />

                <TextField
                  fullWidth
                  size="small"
                  label="Link to Video"
                  value={videoUrl}
                  onChange={event => {
                    setVideoUrl(event.target.value);
                  }}
                  type="video_url"
                  name="video_url"
                />
              </CardContent>
              <CardActions style={{ display: "flex", justifyContent: "center" }}>
                <IconButton onClick={() => { updateDetailsField(null, { name: "video_url", value: videoUrl }); apiInstance.refetchUserData(); setVideoEdit(false) }}><Done /></IconButton>
                {videoEdit ? <IconButton onClick={() => { setVideoEdit(false) }}><Cancel /></IconButton> : null}
              </CardActions>
            </React.Fragment>}
        </Card>
      </Grid>
      <Grid item xs={12}>
        <Paper elevation={5} style={{ marginTop: "10px" }} >
          <Tabs variant="scrollable" value={props.location.pathname}>
            <Tab
              value="/profile"
              component={Link}
              to="/profile"
              label="Details"
            />
            <Tab
              value="/profile/skills"
              component={Link}
              to="/profile/skills"
              label="My Skills"
            />
            <Tab
              value="/profile/work_profile"
              component={Link}
              to="/profile/work_profile"
              label="Work Profile"
            />
            <Tab
              value="/profile/academic"
              component={Link}
              to="/profile/academic"
              label="Acaemics"
            />
            <Tab
              value="/profile/professional"
              component={Link}
              to="/profile/professional"
              label="Professionals"
            />
          </Tabs>
          <Switch>
            <Route path="/profile" exact>
              <form>
                <Grid style={{ padding: "10px" }} container spacing={2}>
                  <Grid item xs={12} style={{ height: "25px" }}></Grid>
                  <Grid item xs={12} md={6}>
                    <TextField
                      fullWidth
                      size="small"
                      label="Alternate Phone"
                      value={altPhone}
                      onBlur={updateDetailsField}
                      onChange={event => {
                        setAltPhone(event.target.value);
                      }}
                      type="phone"
                      name="alt_phone"
                    />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <TextField
                      fullWidth
                      size="small"
                      label="Alternate Email"
                      value={altEmail}
                      onBlur={updateDetailsField}
                      onChange={event => {
                        setAltEmail(event.target.value);
                      }}
                      type="email"
                      name="alt_email"
                    />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <TextField
                      fullWidth
                      size="small"
                      label="Father's Name"
                      onBlur={updateDetailsField}
                      value={fathersName}
                      onChange={event => {
                        setFathersName(event.target.value);
                      }}
                      type="name"
                      name="fathers_name"
                    />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <TextField
                      fullWidth
                      size="small"
                      label="Father's Occupation"
                      value={fathersOccupation}
                      onBlur={updateDetailsField}
                      onChange={event => {
                        setFathersOccupation(event.target.value);
                      }}
                      type="text"
                      name="fathers_occupation"
                    />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <TextField
                      fullWidth
                      size="small"
                      label="Mother's Name"
                      onBlur={updateDetailsField}
                      value={mothersName}
                      onChange={event => {
                        setMothersName(event.target.value);
                      }}
                      type="name"
                      name="mothers_name"
                    />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <TextField
                      fullWidth
                      size="small"
                      label="Mother's Occupation"
                      value={mothersOccupation}
                      onBlur={updateDetailsField}
                      onChange={event => {
                        setMothersOccupation(event.target.value);
                      }}
                      type="text"
                      name="mothers_occupation"
                    />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <TextField
                      fullWidth
                      size="small"
                      label="Date of Birth"
                      onBlur={updateDetailsField}
                      value={dateOfBirth}
                      onChange={event => {
                        setDateOfBirth(event.target.value);
                      }}
                      type="date"
                      name="dateofbirth"
                    />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <FormControl size="small" fullWidth>
                      <InputLabel>Gender</InputLabel>
                      <Select
                        value={gender}
                        onBlur={updateDetailsField}
                        name="gender"
                        onChange={event => {
                          setGender(event.target.value);
                        }}
                      >
                        <MenuItem value="M">Male</MenuItem>
                        <MenuItem value="F">Female</MenuItem>
                        <MenuItem value="C">
                          Trans Genders/Cross Dressers
                        </MenuItem>
                        <MenuItem value="O">Other</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={12} md={4}>
                    <FormControl size="small" fullWidth>
                      <InputLabel>Marital Status</InputLabel>
                      <Select
                        onBlur={updateDetailsField}
                        name="maritalstatus"
                        value={maritalstatus}
                        onChange={event => {
                          setMaritalstatus(event.target.value);
                        }}
                      >
                        <MenuItem value="Married">Married</MenuItem>
                        <MenuItem value="Bachelor">Bachelor</MenuItem>
                        <MenuItem value="Divorcee">Divorcee</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={12} md={4}>
                    <TextField
                      size="small"
                      fullWidth
                      onBlur={updateDetailsField}
                      label="Caste"
                      value={caste}
                      onChange={event => {
                        setCaste(event.target.value);
                      }}
                      type="text"
                      name="caste"
                    />
                  </Grid>
                  <Grid item xs={12} md={4}>
                    <MothertongueSelect
                      onBlur={updateDetailsField}
                      name="mothertongue_id"
                      mothertongue={userDetails.mothertongue_id}
                    />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <TextField
                      fullWidth
                      size="small"
                      label="Aadhaar Number"
                      onBlur={updateDetailsField}
                      value={adhaarNo}
                      onChange={event => {
                        setAdhaarNo(event.target.value);
                      }}
                      type="number"
                      name="aadhar_no"
                    />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <TextField
                      fullWidth
                      size="small"
                      label="Passport Number"
                      onBlur={updateDetailsField}
                      value={passportNo}
                      onChange={event => {
                        setPassportNo(event.target.value);
                      }}
                      type="text"
                      name="passport_no"
                    />
                  </Grid>
                  <Grid item xs={12} md={4}>
                    <FormControl size="small" fullWidth>
                      <InputLabel>Blood Group</InputLabel>
                      <Select
                        onBlur={updateDetailsField}
                        name="bloodgroup"
                        value={bloodgroup}
                        onChange={event => {
                          setBloodgroup(event.target.value);
                        }}
                      >
                        <MenuItem value="A+">A+</MenuItem>
                        <MenuItem value="A-">A-</MenuItem>
                        <MenuItem value="B+">B+</MenuItem>
                        <MenuItem value="B-">B-</MenuItem>
                        <MenuItem value="AB+">AB+</MenuItem>
                        <MenuItem value="AB-">AB-</MenuItem>
                        <MenuItem value="O+">O+</MenuItem>
                        <MenuItem value="O-">O-</MenuItem>
                        <MenuItem value="Hh">Hh</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={12} md={4}>
                    <FormControl size="small" fullWidth>
                      <InputLabel>Physically Challenged</InputLabel>
                      <Select
                        name="physical_challenge"
                        onBlur={updateDetailsField}
                        value={physicalChallenge}
                        onChange={event => {
                          setPhysicalChallenge(event.target.value);
                        }}
                      >
                        <MenuItem value="true">Yes</MenuItem>
                        <MenuItem value="false">No</MenuItem>
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={12} md={4}>
                    <TextField
                      size="small"
                      fullWidth
                      onBlur={updateDetailsField}
                      label="Percentage (PH)"
                      value={percentagePh}
                      onChange={event => {
                        setPercentagePh(event.target.value);
                      }}
                      type="number"
                      name="percentage_ph"
                    />
                  </Grid>
                </Grid>
                <Grid item xs={12} style={{ height: "25px" }}></Grid>
              </form>
            </Route>

            <Route path="/profile/work_profile">
              <form>
                <Grid
                  style={{ padding: "10px" }}
                  container
                  xs={12}
                  justify="center"
                  spacing={2}
                >
                  <Grid item xs={12} md={12} style={{ height: "25px" }}></Grid>
                  <Grid item xs={12} md={12}>
                    <TextField
                      fullWidth
                      size="small"
                      label="Resume Heading"
                      onBlur={updateDetailsField}
                      value={resumeHeading}
                      onChange={event => {
                        setResumeHeading(event.target.value);
                      }}
                      type="text"
                      name="resume_heading"
                    />
                  </Grid>
                  <Grid item xs={12} md={8}>
                    <TextField
                      fullWidth
                      size="small"
                      label="About Me"
                      onBlur={updateDetailsField}
                      value={aboutMe}
                      onChange={event => {
                        setAboutMe(event.target.value);
                      }}
                      type="text"
                      multiline
                      name="about_me"
                    />
                  </Grid>
                  <Grid item xs={12} md={4}>
                    <TextField
                      fullWidth
                      size="small"
                      label="Expected CTC"
                      onBlur={updateDetailsField}
                      value={expectedCTC}
                      onChange={event => {
                        setExpectedCTC(event.target.value);
                      }}
                      type="number"
                      name="expected_ctc"
                    />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <TextField
                      fullWidth
                      size="small"
                      label="Facebook Link"
                      onBlur={updateDetailsField}
                      value={facebookLink}
                      onChange={event => {
                        setFacebookLink(event.target.value);
                      }}
                      type="url"
                      name="facebook_link"
                    />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <TextField
                      fullWidth
                      size="small"
                      label="LinkedIn Link"
                      onBlur={updateDetailsField}
                      value={linkedinLink}
                      onChange={event => {
                        setLinkedInLink(event.target.value);
                      }}
                      type="url"
                      name="linkedin_link"
                    />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <TextField
                      fullWidth
                      size="small"
                      label="Google Link"
                      onBlur={updateDetailsField}
                      value={googleLink}
                      onChange={event => {
                        setGoogleLink(event.target.value);
                      }}
                      type="url"
                      name="google_link"
                    />
                  </Grid>
                  <Grid item xs={12} md={6}>
                    <TextField
                      fullWidth
                      size="small"
                      label="Website"
                      onBlur={updateDetailsField}
                      value={website}
                      onChange={event => {
                        setWebsite(event.target.value);
                      }}
                      type="url"
                      name="website"
                    />
                  </Grid>
                </Grid>
              </form>
            </Route>
            <Route path="/profile/academic">
              <Academic />
            </Route>
            <Route path="/profile/professional">
              <Professional />
            </Route>
            <Route path="/profile/skills">
              <Skill />
            </Route>
          </Switch>
        </Paper>
      </Grid>
      <Grid item md={4}>
        <Snackbar
          open={showError}
          autoHideDuration={6000}
          onClose={() => {
            showErrorSet(false);
          }}
        >
          <MuiAlert
            variant="filled"
            onClose={() => {
              showErrorSet(false);
            }}
            severity="error"
          >
            {error}
          </MuiAlert>
        </Snackbar>
      </Grid>
    </Grid>
  );
});

class Profile extends React.Component {
  userDetails = {};
  loaded = false;
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    apiInstance
      .requestWithAuth("private/select_user_details", {})
      .then(result => {
        if (result.status === 1) {
          result.dateofbirth = yyyymmdd(
            result.dateofbirth ? new Date(result.dateofbirth) : new Date()
          );
          this.userDetails = JSON.stringify(result);
          this.loaded = true;
          this.forceUpdate();
          //TODO: add more
        }
      });
  }
  render() {
    return (
      <Grid container spacing={1}>
        <Grid item xs={12} md={12}>
          {this.loaded ? <ProfileComponent data={this.userDetails} /> : null}
        </Grid>
      </Grid>
    );
  }
}
export { ProfileCompact, Profile, CitySelect, MothertongueSelect, yyyymmdd };
