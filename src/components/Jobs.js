import {
  Grid,
  Typography,
  Button,
  Card,
  CardHeader,
  CardContent,
  CardActions,
  CardMedia,
  Tabs,
  Tab,
  Collapse,
  Chip,
  IconButton
} from "@material-ui/core";
import React, { useState } from "react";
import "./styles.css";
import apiInstance from "../api";
import { yyyymmdd } from "./";
import { withRouter, Route, Link, Switch } from "react-router-dom";
import { Timer, Work, School, LocationCity, ExpandLess, ExpandMore } from "@material-ui/icons";

function urlSanitize(url) {
  return url.startsWith("http:") || url.startsWith("https:") ? url : "http://" + url;
}
function JobCard(props) {
  const data = props.data,
    apply = !props.applied,
    callback = props.callback;
  const [expanded, expandedUpdate] = useState(false);
  function action() {
    (async () => {
      let response = await apiInstance.requestWithAuth(
        "private/dynamic_challenge_apply",
        { id: data.challenge_id, operation: apply ? "insert" : "delete" }
      );
      await callback();
    })();
  }
  return (
    <Card style={{ padding: "10px" }}>
      <CardHeader title={data.title} subheader={data.org_name} />
      <CardContent>
        {data.skills.map(skill => <Chip style={{ margin: "5px" }} label={skill.name} />)}
      </CardContent>
      <CardActions style={{ display: "flex", justifyContent: "space-between" }}>
        <Button color="primary" href={urlSanitize(data.external_link)} target="_blank">
          APPLY
          </Button>
        <IconButton onClick={() => { expandedUpdate(!expanded) }}>
          {expanded ? <ExpandLess /> : <ExpandMore />}
        </IconButton>
      </CardActions>
      <Collapse in={expanded}>

        <CardContent>
          <Grid container spacing={1}>
            <Grid item xs={6}>
              <Timer />
              <Typography variant="body1">
                {yyyymmdd(new Date(data.end_date))}
              </Typography>

            </Grid>
            <Grid item xs={6}>
              <Work />
              <Typography variant="body1">
                {data.experience} years
              </Typography>

            </Grid>
            <Grid item xs={6}>
              <School />
              <Typography variant="body1">
                {data.min_qualification_name}
              </Typography>

            </Grid>
            <Grid item xs={6}>
              <LocationCity />
              <Typography variant="body1">
                {data.city_name}, {data.state_name}, {data.country_name}
              </Typography>

            </Grid>
          </Grid>
        </CardContent>
        <CardActions>
          <Button color="primary" href={urlSanitize(data.org_website)} target="_blank">
            OPEN WEBSITE
          </Button>
        </CardActions>
      </Collapse>
    </Card>
  );
}

class Jobs extends React.Component {
  category;
  constructor(props) {
    super(props);
    this.category = props.category ? props.category : 0;
    this.state = {
      challengesAvaliable: []
    };
    this.setState = this.setState.bind(this);
    this.handleListClick = this.handleListClick.bind(this);
  }
  componentDidMount() {
    this.handleListClick();
  }
  handleListClick() {
    (async () => {
      let response = await apiInstance.requestExternal(
        "http://18.224.31.58:3200/b2cjob/job-list/",
        {}
      );
      if (response && response.data) {
        this.setState({ challengesAvailable: response.data });
      } else this.setState({ challengesAvailable: [] });
    })();
  }
  render() {
    return (
      <Grid container spacing={2}>
        {this.state.challengesAvailable &&
          this.state.challengesAvailable.length > 0
          ? this.state.challengesAvailable.map(challengeAvailable => {
            return (
              <Grid item xs={12} sm={6} md={4} lg={3}>
                <JobCard
                  callback={this.handleListClick}
                  data={challengeAvailable}
                />
              </Grid>
            );
          })
          : null}
      </Grid>
    );
  }
}
export { Jobs };
