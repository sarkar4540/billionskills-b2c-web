import React, { useState, useRef } from "react";
import {
  Grid,
  Paper,
  Typography,
  Avatar,
  Button,
  Divider,
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Modal,
  TextField,
  Snackbar,
  IconButton
} from "@material-ui/core";
import MuiAlert from "@material-ui/lab/Alert";
import { Edit, Delete, Lock, Done, Cancel, CloudUpload } from "@material-ui/icons";
import { Link, withRouter } from "react-router-dom";
import apiInstance from "../api";
import Cropper from 'react-cropper';
import 'cropperjs/dist/cropper.css';

const Account = withRouter(props => {
  const [error, setError] = useState("");
  const [showError, showErrorSet] = useState(false);
  const [newPass, setNewPass] = useState("");
  const [showPic, showPicSet] = useState(false);
  const [pic, setPic] = useState(apiInstance.getUserData().photo_lg);
  const [tpic, setTPic] = useState(pic);
  const [pass, setPass] = useState("");
  const [passwordPanelHidden, setPasswordPanelHidden] = useState(true);
  const fileUpload = useRef(null);
  const cropper = useRef(new Cropper());
  function handleChangePassword() {
    setPasswordPanelHidden(false);
  }
  function handleCloseChangePassword() {
    setPasswordPanelHidden(true);
  }
  async function updateDetailsField() {
    const tpic_can = cropper.current.getCroppedCanvas({ width: "256px" }).toDataURL(),
      tpics_can = cropper.current.getCroppedCanvas({ width: "128px" }).toDataURL();
    await apiInstance.requestWithAuth("private/update_user_detail", {
      field_name: "photo_sm",
      field_data: tpics_can
    });
    await apiInstance.requestWithAuth("private/update_user_detail", {
      field_name: "photo_lg",
      field_data: tpic_can
    });
    await apiInstance.refetchUserData();
    setPic((await apiInstance.getUserData()).photo_lg);
  }
  function handleSaveChangePassword() {
    apiInstance
      .requestWithAuth("private/change_password", {
        new_password: newPass,
        password: pass
      })
      .then(value => {
        if (value.status === 1) {
          setError(
            "Password was successfully updated. You will be logged out in 3 seconds. Please Log in again."
          );
          apiInstance.logOut();
          setTimeout(() => {
            props.history.push("/");
          }, 3000);
        } else if (value.status === 4) {
          setError(value.error);
        } else {
          setError("Password was not updated.");
        }
        showErrorSet(true);
      });
  }

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Paper elevation={2}>
          <Grid container alignItems="center">
            <Grid item xs={12} md={6}>
              {showPic ? <React.Fragment>
                <Cropper
                  ref={cropper}
                  src={tpic}
                  style={{
                    position: "relative",
                    margin: "10px auto",
                    width: "100%",
                    height: "256px"
                  }}
                  // Cropper.js options
                  aspectRatio={1}
                  guides={false} />
                <IconButton onClick={async () => {
                  updateDetailsField(); showPicSet(false);
                }}><Done />
                </IconButton><IconButton onClick={async () => { showPicSet(false) }}><Cancel />
                </IconButton></React.Fragment> : <React.Fragment>
                  <Avatar
                    style={{
                      position: "relative",
                      margin: "10px auto",
                      width: "256px",
                      height: "256px"
                    }}
                    src={pic}
                  ></Avatar>
                  {pic ? <IconButton onClick={async () => { showPicSet(true); }}><Edit />
                  </IconButton> : null}
                  <IconButton onClick={async () => { await fileUpload.current.click(); }}><CloudUpload />
                  </IconButton></React.Fragment>}
              <input type="file" ref={fileUpload} style={{ display: "none" }} onChange={async (evt) => {
                var reader = new FileReader();
                var file = evt.target.files[0];

                reader.onload = async function (upload) {
                  await setTPic(upload.target.result);
                  showPicSet(true);
                };
                reader.readAsDataURL(file);
              }} />


            </Grid>
            <Grid item xs={12} md={6}>
              <Typography variant="h4">
                {apiInstance.getUserData().first_name + " " + apiInstance.getUserData().last_name}
              </Typography>
              <Typography
                variant="h5"
                style={{
                  whiteSpace: "nowrap",
                  overflow: "hidden",
                  textOverflow: "ellipsis"
                }}
              >
                {apiInstance.getUserData().email}
              </Typography>
              <Divider />

              <Button
                fullWidth
                startIcon={<Lock />}
                component={Link}
                onClick={handleChangePassword}
                hidden={passwordPanelHidden}
              >
                Change Password
              </Button>
              <Modal open={!passwordPanelHidden}>
                <Paper
                  style={{
                    width: "400px",
                    padding: "5px",
                    position: "relative",
                    top: "50%",
                    left: "50%",
                    transform: "translate(-50%,-50%)",
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center"
                  }}
                >
                  <Typography variant="h4">Change Password</Typography>
                  <TextField
                    fullWidth
                    label="Old Password"
                    size="small"
                    value={pass}
                    onChange={event => {
                      setPass(event.target.value);
                    }}
                    type="password"
                    name="password"
                  />
                  <TextField
                    fullWidth
                    label="New Password"
                    size="small"
                    value={newPass}
                    onChange={event => {
                      setNewPass(event.target.value);
                    }}
                    type="password"
                    name="new_password"
                  />
                  <Button
                    fullWidth
                    onClick={handleSaveChangePassword}
                    color="primary"
                  >
                    Save
                  </Button>
                  <Button
                    fullWidth
                    onClick={handleCloseChangePassword}
                    color="default"
                  >
                    Cancel
                  </Button>
                </Paper>
              </Modal>
              <Button
                fullWidth
                onClick={() => {
                  apiInstance.logOut();
                  setTimeout(() => {
                    document.location.href = "/";
                  }, 1000);
                }}
              >
                Log Out
              </Button>
              <Divider />
              <Button
                fullWidth
                startIcon={<Delete />}
                style={{ color: "red" }}
                component={Link}
                to="#"
              >
                Delete Account
              </Button>
            </Grid>
          </Grid>
        </Paper>
      </Grid>
      <Grid item xs={12} md={6}>
        <Card elevation={2}>
          <CardHeader
            title="GraphWeb"
            subheader="Publish your Portfolio over web."
          />
          <CardContent>
            Get an amazing personal website and portfolio using your
            BillionSkills account.
              </CardContent>
          <CardActions>
            <Button color="primary">Subscribe</Button>
            <Button>Know More</Button>
          </CardActions>
        </Card>
      </Grid>
      <Grid item xs={12} md={6}>
        <Card elevation={2}>
          <CardHeader
            title="Conclave"
            subheader="Organize events, easily."
          />
          <CardContent>
            Organize, invite and manage events, like conferences, programmes
            and workshops.
              </CardContent>
          <CardActions>
            <Button color="primary">Subscribe</Button>
            <Button>Know More</Button>
          </CardActions>
        </Card>
      </Grid>

      <Snackbar
        open={showError}
        autoHideDuration={6000}
        onClose={() => {
          showErrorSet(false);
        }}
      >
        <MuiAlert
          variant="filled"
          onClose={() => {
            showErrorSet(false);
          }}
          severity="error"
        >
          {error}
        </MuiAlert>
      </Snackbar>
    </Grid>
  );
});

export { Account };
