import {
  Grid,
  Typography,
  Button,
  Card,
  CardHeader,
  CardContent,
  CardActions,
  CardMedia,
  IconButton,
  TextField,
  CardActionArea,
  Modal,
  Paper,
  Divider,
  Collapse,
  Tab,
  Tabs
} from "@material-ui/core";
import React, { useState } from "react";
import "./styles.css";
import apiInstance from "../api";
import { Delete, Label, PostAdd, Edit, Done, Cancel, ExpandLess, ExpandMore } from "@material-ui/icons";
import { ImageUpload } from "./";
import { yyyymmdd } from "./Profile";

import CKEditor from "@ckeditor/ckeditor5-react";
import ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import { withRouter, Link, Switch, Route } from "react-router-dom";

function PostPanel(props) {
  const [title, titleUpdate] = useState("");
  const [data, dataUpdate] = useState("");
  const [url, urlUpdate] = useState("");
  const callback = props.callback;
  function action() {
    (async () => {
      let response = await apiInstance.requestWithAuth("private/dynamic_post", {
        id: 0,
        operation: "insert",
        title: title,
        data: data,
        url: url
      });
      if (response && !response.error_msg) {
        dataUpdate(" ");
        titleUpdate("");
        urlUpdate("");
        if (callback) await callback();
      }
    })();
  }
  return (
    <Card>
      <CardContent>
        <Grid direction="column" spacing="1" container>
          <Grid item xs>
            <Typography variant="h5" align="center">Compose Article</Typography></Grid>
          <Grid item xs><TextField
            fullWidth
            label="Article Title"
            value={title}
            onChange={e => {
              titleUpdate(e.target.value);
            }}
          />
          </Grid>
          <Grid item xs>
            <Typography variant="h7" align="center">Article Content</Typography>
            <CKEditor
              data={data}
              onChange={(event, editor) => {
                dataUpdate(editor.getData());
              }}
              editor={ClassicEditor}
            />
          </Grid>
          <Grid item xs><TextField
            fullWidth
            label="Reference Link"
            value={url}
            onChange={e => {
              urlUpdate(e.target.value);
            }}
          />
          </Grid>
        </Grid>



      </CardContent>
      <CardActions style={{ display: "flex", justifyContent: "space-evenly" }}>
        <IconButton color="primary"
          onClick={e => action}
        >
          <Done />
        </IconButton>
        <IconButton
          onClick={props.onClose}
        >
          <Cancel />
        </IconButton>
      </CardActions>
    </Card>
  );
}

function PostCard(props) {
  const data = props.data,
    callback = props.callback,
    self = props.self;
  const [edit, editUpdate] = useState(false);
  const [editData, editDataUpdate] = useState(props.data.data);
  const [expanded, expandedUpdate] = useState(false);
  function deleteAction() {
    (async () => {
      let response = await apiInstance.requestWithAuth("private/dynamic_post", {
        id: data.id,
        operation: "delete",
        ...data
      });
      await callback();
    })();
  }
  function updateAction() {
    (async () => {
      let response = await apiInstance.requestWithAuth("private/dynamic_post", {
        id: data.id,
        operation: "update",
        data: editData,
        ...data
      });
      editUpdate(false);
      await callback();
    })();
  }
  return (
    <Card style={{ padding: "10px" }}>
      <CardHeader
        title={data.title}
        subheader={data.first_name + " " + data.last_name + " - " + yyyymmdd(new Date(data.create_date_time))}
      />
      <CardActions style={{ display: "flex", justifyContent: "space-between" }}>
        <IconButton onClick={() => { expandedUpdate(!expanded) }}>
          {expanded ? <ExpandLess /> : <ExpandMore />}
        </IconButton>
        {self && !edit ? (
          <div>
            <IconButton
              onClick={e => {
                e.stopPropagation();
                deleteAction();
              }}
            >
              <Delete color="red" />
            </IconButton>
            <IconButton
              onClick={e => {
                e.stopPropagation();
                expandedUpdate(true);
                editUpdate(true);
                editDataUpdate(data.data);
              }}
            >
              <Edit />
            </IconButton>
          </div>
        ) : null}
      </CardActions>
      <Collapse in={expanded} unmountOnExit>
        {edit ? <React.Fragment>
          <CardContent>
            <CKEditor
              data={editData}
              onChange={(event, editor) => {
                editDataUpdate(editor.getData());
              }}
              editor={ClassicEditor}
            />
          </CardContent>

          <CardActions style={{ display: "flex", justifyContent: "space-evenly" }}>
            <IconButton
              onClick={e => {
                e.stopPropagation();
                updateAction();
              }}
            >
              <Done />
            </IconButton>
            <IconButton
              onClick={e => {
                e.stopPropagation();
                editUpdate(false);
              }}
            >
              <Cancel />
            </IconButton>
          </CardActions></React.Fragment> :
          <CardContent style={{ textAlign: "left" }} className="post-card" dangerouslySetInnerHTML={{ __html: data.data }}></CardContent>}
      </Collapse>
    </Card>
  );
}

class PostComponent extends React.Component {
  category;
  constructor(props) {
    super(props);
    this.props = props;
    this.userId = props.userId ? props.userId : 0;
    this.self = props.self;
    this.state = {
      open: false,
      posts: [],
      all_posts: []
    };
    this.setState = this.setState.bind(this);
    this.handleListClick = this.handleListClick.bind(this);
  }
  componentDidMount() {
    this.handleListClick();
  }
  handleListClick() {
    (async () => {
      let response = await apiInstance.requestWithAuth("private/user_posts", {
        user_id: this.userId
      });
      if (response && !response.status && !response.error_msg) {
        console.log(response);
        this.setState({ posts: response });
      } else this.setState({ posts: [] });
    })();
    (async () => {
      let response = await apiInstance.requestWithAuth("private/all_posts", {
        user_id: this.userId
      });
      if (response && !response.status && !response.error_msg) {
        console.log(response);
        this.setState({ all_posts: response });
      } else this.setState({ all_posts: [] });
    })();
  }
  render() {
    return (
      <Grid container spacing={2}>
        <Grid item xs={12} sm={8}>
          <Tabs variant="scrollable" value={this.props.location.pathname}>
            <Tab
              value="/posts"
              component={Link}
              to="/posts"
              label="All"
            />
            <Tab
              value="/posts/my"
              component={Link}
              to="/posts/my"
              label="My Articles"
            />
          </Tabs>
        </Grid>

        <Grid item xs={12} sm={4} style={{ display: "flex", justifyContent: "flex-end" }}>
          <IconButton color="primary"
            onClick={() => {
              this.setState({ open: true });
            }}>
            <PostAdd />
          </IconButton>
          <Modal
            open={this.state.open}
            onClose={() => {
              this.setState({ open: false });
            }}
          >
            <Paper
              style={{
                maxWidth: "1024px",
                maxHeight: "100%",
                overflowY: "auto",
                top: "50%",
                left: "50%",
                transform: "translate(-50%,-50%)",
                position: "relative"
              }}
            >
              <PostPanel
                callback={this.handleListClick}
                onClose={() => {
                  this.setState({ open: false });
                }}
              />
            </Paper>
          </Modal>
        </Grid>
        <Switch location={this.props.location}>
          <Route path='/posts/my'>
            {this.state.posts && this.state.posts.length > 0
              ? this.state.posts.map(post => {
                return (
                  <Grid item xs={12} md={6}>
                    <PostCard
                      data={post}
                      callback={this.handleListClick}
                      self={this.self}
                    />
                  </Grid>
                );
              })
              : null}</Route>
          <Route path='/posts' exact>
            {this.state.all_posts && this.state.all_posts.length > 0
              ? this.state.all_posts.map(post => {
                return (
                  <Grid item xs={12} md={6}>
                    <PostCard
                      data={post}
                      callback={this.handleListClick}
                    />
                  </Grid>
                );
              })
              : null}</Route>
        </Switch>
      </Grid>
    );
  }
}
const Posts = withRouter(PostComponent);
export { Posts, PostCard };
