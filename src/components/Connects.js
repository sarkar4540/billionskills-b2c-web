import {
  Paper,
  TextField,
  Grid,
  IconButton,
  InputAdornment,
  Avatar,
  Typography,
  Button
} from "@material-ui/core";
import React from "react";
import "./styles.css";
import { Search, CompareArrows, Done, Facebook, LinkedIn, Public } from "@material-ui/icons";
import apiInstance from "../api";
import ReactPlayer from "react-player";
function UserDetails(props) {
  const data = props.data;
  const callback = props.callback;

  const action = value => {
    (async () => {
      await apiInstance.requestWithAuth("private/dynamic_connect", value);
      if (callback) await callback();
    })();
  };
  return (
    <Paper>
      <div
        style={{
          padding: "5px",
          display: "flex",
          flexDirection: "column",
          alignItems: "center"
        }}
      >
        <div style={{ display: "flex", justifyItems: "center" }}><Avatar src={data.photo_sm} style={{ height: "128px", width: "128px" }}></Avatar></div>
        <Typography variant="h5">
          {data.first_name} {data.last_name}
        </Typography>
        <Typography variant="h6">{data.email}</Typography>
        <Typography variant="caption">{data.resume_heading}</Typography>
        <Typography variant="body1">{data.about_me}</Typography>
        <Typography variant="body1">
          {data.match_count ? data.match_count : 0} common skills.
      </Typography>
        <div>
          {data.website ? <IconButton href={data.website} target="_blank">
            <Public />
          </IconButton> : null}
          {data.facebook_link ? <IconButton href={data.facebook_link} target="_blank">
            <Facebook />
          </IconButton> : null}
          {data.linkedin_link ? <IconButton href={data.linkedin_link} target="_blank">
            <LinkedIn />
          </IconButton> : null}
        </div>
        {data.video_url ?
          <ReactPlayer
            width="100%"
            height="256px"
            url={data.video_url}
          /> : null}
        <div>
          <Button
            onClick={() => {
              action({ operation: data.fuser_id ? "delete" : "insert", user_id: data.id });
            }}
            color=
            {data.fuser_id ? "secondary" : "primary"}
            startIcon={data.fuser_id ? <Done /> : <CompareArrows />}
          >
            {data.fuser_id ? "Connected" : "Connect"}
          </Button>
        </div>
      </div>
    </Paper>
  );
}
class Connects extends React.Component {
  constructor(props) {
    super(props);
    this.state = { nameField: "", users: [], userDetails: null };
    this.setState = this.setState.bind(this);
    this.loadUsers = this.loadUsers.bind(this);
  }
  loadUsers(value) {
    (async () => {
      let response = await apiInstance.requestWithAuth("private/user_search", {
        name: value
      });
      if (
        response &&
        !response.status &&
        response.status != 0 &&
        !response.error_msg
      ) {
        console.log(response);
        this.setState({ users: response });
      } else this.setState({ users: [] });
    })();
  }
  componentDidMount() {
    this.loadUsers("");
  }
  render() {
    return (
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Paper style={{ padding: "5px" }}>
            <TextField
              label="Filter by Name"
              name="name_field"
              value={this.state.nameField}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="start">
                    <Search />
                  </InputAdornment>
                )
              }}
              onChange={e => {
                this.setState({ nameField: e.target.value });
                this.loadUsers(e.target.value);
              }}
              fullWidth
            />
          </Paper>
        </Grid>
        {this.state.users.map(user => (
          <Grid item xs={12} md={6} lg={4}>
            <UserDetails
              data={user}
              callback={() => this.loadUsers(this.state.nameField)}
              action={() => { this.setState({ userDetails: user }) }}
            />
          </Grid>
        ))}
      </Grid>
    );
  }
}

export { Connects };

/**
 * SELECT *, COUNT((SELECT skill_id from public.b2c_user_skill e2 WHERE e2.user_id=e1.id INTERSECT SELECT skill_id FROM public.b2c_user_skill e3 WHERE e3.id=4)) match_count FROM public.user_details e1 GROUP BY e1.id ORDER BY match_count DESC;
 */
