import { Grid, Paper, Button, Tab, Tabs } from "@material-ui/core";
import React from "react";
import "./styles.css";
import { Project } from "./Projects";
import { Publication } from "./Publications";
import { Research } from "./Research";
import { Award } from "./Awards";
import { Hobby } from "./Hobby";
import { withRouter, Route, Switch, Link } from "react-router-dom";

let Portfolio = withRouter(props => {
  let route_props = location => {
    return {
      component: Link,
      variant: props.location.pathname === location ? "contained" : "initial",
      to: location,
      color: "secondary"
    };
  };
  return (
    <Grid container spacing={2}>
      <Grid item xs={12} md={12}>
        <Tabs variant="scrollable" value={props.location.pathname}>
          <Tab
            value="/portfolio"
            component={Link}
            to="/portfolio"
            label="Research"
          />
          <Tab
            value="/portfolio/awards"
            component={Link}
            to="/portfolio/awards"
            label="Awards"
          />
          <Tab
            value="/portfolio/projects"
            component={Link}
            to="/portfolio/projects"
            label="Projects"
          />
          <Tab
            value="/portfolio/publications"
            component={Link}
            to="/portfolio/publications"
            label="Publications"
          />
          <Tab
            value="/portfolio/hobbies"
            component={Link}
            to="/portfolio/hobbies"
            label="Hobbies"
          />
        </Tabs>
      </Grid>
      <Grid item xs={12} md={12}>
        <Switch>
          <Route path="/portfolio" exact>
            <Research />
          </Route>
          <Route path="/portfolio/awards">
            <Award />
          </Route>
          <Route path="/portfolio/projects">
            <Project />
          </Route>
          <Route path="/portfolio/publications">
            <Publication />
          </Route>
          <Route path="/portfolio/hobbies">
            <Hobby />
          </Route>
        </Switch>
      </Grid>
    </Grid>
  );
});

export { Portfolio };
