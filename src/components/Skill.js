import {
  Paper,
  Typography,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  IconButton
} from "@material-ui/core";
import { HighlightOff, Add, Remove, Cancel } from "@material-ui/icons";
import React from "react";
import "./styles.css";
import apiInstance from "../api";
class Skill extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error_msg: null,
      skills: [],
      all_skills: [],
      showAdd: false
    };
    this.user_id = props.userId ? props.userId : 0;
    this.type = props.type ? props.type : 0;
    this.type_id = props.typeId ? props.typeId : 0;
    this.setState = this.setState.bind(this);
    this.handleRemove = this.handleRemove.bind(this);
    this.handleAdd = this.handleAdd.bind(this);
    this.handleSubmit = (props.onChange
      ? props.onChange
      : async skills => {
          let data = { skill_id: [] };
          await skills.forEach(skill => {
            data.skill_id.push(skill.id);
          });
          await apiInstance.requestWithAuth("private/update_skill", data);
          await this.refresh();
        }
    ).bind(this);
  }
  async refresh() {
    let response = await apiInstance.requestWithAuth("private/user_skill", {
      type: this.type,
      type_id: this.type_id
    });
    if (!response.status && !response.error_msg) {
      this.setState({ skills: response });
    } else this.setState({ skills: [] });
  }
  componentDidMount() {
    (async () => {
      (async () => {
        await this.refresh();
        let response2 = await apiInstance.request("public/skills", {});
        if (!response2.status && !response2.error_msg) {
          /*
                    let all_skills=[];
                    response2.forEach(()=>{
                        if(response.indexOf({response2.}))
                    })
                    this.setState({all_skills:all_skills});
                    */
          this.setState({ all_skills: response2 });
        } else this.setState({ all_skills: [] });
      })();
    })();
  }
  handleRemove(data) {
    data.error_msg = null;
    this.setState(data);
  }
  handleAdd(data) {
    let skills = this.state.skills;
    skills.push(data);
    this.setState({
      skills: skills
    });
    this.handleSubmit(skills);
  }
  async handleDelete(data) {
    let skills = this.state.skills;
    const index = skills.indexOf(data);
    if (index > -1) {
      await skills.splice(index, 1);
    }
    this.setState({
      skills: skills
    });
    await this.handleSubmit(skills);
  }
  render() {
    return (
      <div>
        <div
          style={{
            display: "flex",
            verticalAlign: "center",
            alignItems: "middle"
          }}
        >
          <div
            style={{
              padding: "5px",
              alignItems: "left",
              display: "inline-block"
            }}
          >
            {this.state.skills.length > 0
              ? this.state.skills.map(skill => {
                  return (
                    <Paper
                      style={{
                        margin: "5px",
                        display: "inline-block",
                        paddingLeft: "5px"
                      }}
                    >
                      <Typography display="inline" variant="body1">
                        {skill.name}
                      </Typography>
                      <IconButton
                        onClick={() => {
                          this.handleDelete(skill);
                        }}
                        aria-label="delete"
                      >
                        <Cancel fontSize="small" />
                      </IconButton>
                    </Paper>
                  );
                })
              : null}
          </div>
          <div style={{ display: "inline-block" }}>
            <Paper style={{ display: "inline-block", margin: "5px" }}>
              <IconButton
                style={{ display: "inline" }}
                onClick={() => {
                  this.setState({ showAdd: !this.state.showAdd });
                }}
              >
                <Add variant="small" />
              </IconButton>
              {this.state.showAdd ? (
                <FormControl
                  style={{
                    margin: "5px",
                    width: "125px",
                    display: "inline-block"
                  }}
                  variant="outlined"
                  size="small"
                >
                  <InputLabel>Add Skill</InputLabel>
                  <Select
                    onChange={async event => {
                      await this.handleAdd(event.target.value);
                    }}
                    fullWidth
                  >
                    {this.state.all_skills.length > 0
                      ? this.state.all_skills.map(skill => {
                          return (
                            <MenuItem value={skill} key="skill_id">
                              {skill.name}
                            </MenuItem>
                          );
                        })
                      : null}
                  </Select>
                </FormControl>
              ) : null}
            </Paper>
          </div>
        </div>
      </div>
    );
  }
}

export { Skill };
