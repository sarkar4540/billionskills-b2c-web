import {
  Paper,
  Grid,
  IconButton,
  Link,
  Button,
  Avatar,
  Typography,
  List,
  Divider,
  Chip
} from "@material-ui/core";
import React from "react";
import "./styles.css";
import apiInstance from "../api";
import { Edit, CloudDownload, ArrowForward } from "@material-ui/icons";

class UserDetails extends React.Component {
  constructor(props) {
    super(props);
    this.userId = props.userId;
    this.state = {
      userDetails: {},
      professional: {},
      skills: []
    };
    this.setState = this.setState.bind(this);
  }
  componentDidMount() {
    (async () => {
      let result = await apiInstance.requestWithAuth(
        "private/select_user_details",
        { userId: this.userId }
      );
      let response = await apiInstance.requestWithAuth("private/professional", {
        id: 0
      });
      let response2 = await apiInstance.requestWithAuth(
        "private/user_skill",
        {}
      );
      if (response && response2 && result && !result.error_msg) {
        this.setState({
          professional: response[0],
          userDetails: result,
          skills: response2
        });
        //TODO: add more
      } else {
        console.dir({ response, response2, result });
      }
    })();
  }
  render() {
    return (
      <Paper style={{ padding: "5px" }} elevation={5}>
        <Grid container direction="column" alignItems="stretch" spacing={1}>
          <Grid item container justify="flex-end">
            <IconButton component={Link} to="/profile">
              <Edit />
            </IconButton>
          </Grid>
          <Grid item container justify="center">
            <Avatar
              src="/profile.jpg"
              style={{ width: "128px", height: "128px" }}
            ></Avatar>
          </Grid>
          <Grid item>
            <Typography variant="h4">
              {this.state.userDetails.first_name}{" "}
              {this.state.userDetails.last_name}
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant="body1">
              {this.state.userDetails.resume_heading}
            </Typography>
            <Typography variant="body2">
              {this.state.professional.company}
            </Typography>
          </Grid>
          <Grid item container justify="space-evenly">
            <Button
              color="primary"
              startIcon={<CloudDownload />}
              variant="contained"
              size="small"
              href="#"
              target="_blank"
            >
              Resume
            </Button>
            <Button
              color="primary"
              size="small"
              startIcon={<List />}
              component={Link}
              to="/portfolio"
            >
              Portfolio
            </Button>
          </Grid>
          <Grid item>
            <Divider />
          </Grid>
          <Grid item container>
            <Grid item xs={10} container justify="center">
              <Typography variant="h5">My Skills</Typography>
            </Grid>
            <Grid item xs={2} container justify="flex-end">
              <IconButton component={Link} to="/profile/skills">
                <Edit />
              </IconButton>
            </Grid>
          </Grid>
          <Grid item container spacing={1} justify="space-evenly">
            {this.state.skills.map(skill => (
              <Grid item>
                <Chip color="secondary" label={skill.name} />
              </Grid>
            ))}
          </Grid>
          <Grid item container justify="flex-end">
            <IconButton component={Link} to="/view_profile">
              <ArrowForward />
            </IconButton>
          </Grid>
        </Grid>
      </Paper>
    );
  }
}

export { UserDetails };
