class API {
  loginInfo = null;

  constructor() {
    let info = localStorage.getItem("user-data");
    if (info !== null) {
      this.loginInfo = JSON.parse(info);
    }
  }

  getUserData() {
    return this.loginInfo;
  }
  loading = 0;
  loadingUpdateCallback = null;
  startLoading(k) {
    this.loading++;
    if (this.loading === 1 && this.loadingUpdateCallback)
      this.loadingUpdateCallback(true);
  }
  stopLoading(k) {
    this.loading--;
    if (this.loading === 0 && this.loadingUpdateCallback)
      this.loadingUpdateCallback(false);
  }
  setLoadingUpdateCallback(callback) {
    this.loadingUpdateCallback = callback;
  }
  getStaticUrl(url) {
    if (url.startsWith("http://") || url.startsWith("https://")) return url;
    else return "http://" + window.location.hostname + ":3000/uploads/" + url;
  }
  async request(path, data) {
    this.startLoading("r1");
    return await fetch("http://" + window.location.hostname + ":3000/" + path, {
      method: "POST", // *GET, POST, PUT, DELETE, etc.
      mode: "cors", // no-cors, *cors, same-origin
      cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
      headers: {
        "Content-Type": "application/json"
      },
      redirect: "follow", // manual, *follow, error
      body: JSON.stringify(data) // body data type must match "Content-Type" header
    }).then(async response => {
      this.stopLoading("r1");
      if (response.status === 200) {
        return await response.json();
      } else {
        return {
          status: 4,
          error: await response.text()
        };
      }
    });
  }
  async requestExternal(path, data) {
    this.startLoading("r1");
    var form_data = new FormData();

    for (var key in data) {
      form_data.append(key, data[key]);
    }
    return await fetch(path + "?" + new URLSearchParams(form_data).toString(), {
      method: "GET",  // no-cors, *cors, same-origin
      cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
      redirect: "follow"
    }).then(async response => {
      this.stopLoading("r1");
      if (response.status === 200) {
        return await response.json();
      } else {
        return {
          status: 4,
          error: await response.text()
        };
      }
    });
  }
  isLoggedIn() {
    return this.loginInfo !== null;
  }
  logOut() {
    localStorage.removeItem("user-data");
    this.loginInfo = null;
  }
  async logIn(email, password) {
    this.startLoading("li1");
    return await this.request("private/login", {
      email: email,
      password: password
    }).then(async response => {
      this.stopLoading("li1");
      if (response.status === 1) {
        this.loginInfo = response;
        this.loginInfo.isLoggedIn = true;
        localStorage.setItem("user-data", JSON.stringify(response));
      }
      return response;
    });
  }
  async refetchUserData() {
    this.startLoading("li1");
    return await this.requestWithAuth("private/select_user_details", {
    }).then(async response => {
      this.stopLoading("li1");
      if (response.status === 1) {
        response.auth_token = this.loginInfo.auth_token;
        response.user_id = this.loginInfo.user_id;
        this.loginInfo = response;
        localStorage.setItem("user-data", JSON.stringify(response));
      }
      return response;
    });
  }
  async requestWithAuth(path, data, formData = false) {
    if (this.isLoggedIn()) {
      this.startLoading("rwa1");
      return await fetch(
        "http://" + window.location.hostname + ":3000/" + path,
        {
          method: "POST", // *GET, POST, PUT, DELETE, etc.
          mode: "cors", // no-cors, *cors, same-origin
          cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
          headers: {
            "Content-Type": formData ? "multipart/form-data" : "application/json",
            "auth-token": this.loginInfo.auth_token,
            "user-id": this.loginInfo.user_id
          },
          redirect: "follow", // manual, *follow, error
          body: JSON.stringify(data) // body data type must match "Content-Type" header
        }
      ).then(async response => {
        this.stopLoading("rwa1");
        if (response.status === 200) {
          return await response.json();
        } else if (response.status === 401) {
          this.startLoading("rwa2");
          return await fetch(
            "http://" +
            window.location.hostname +
            ":3000/private/regenerate_token",
            {
              method: "POST", // *GET, POST, PUT, DELETE, etc.
              mode: "cors", // no-cors, *cors, same-origin
              cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
              headers: {
                "Content-Type": "application/json",
                "auth-token": this.loginInfo.auth_token,
                "user-id": this.loginInfo.user_id
              },
              redirect: "follow", // manual, *follow, error
              body: JSON.stringify({}) // body data type must match "Content-Type" header
            }
          ).then(async response2 => {
            this.stopLoading("rwa2");
            if (response2.status === 200) {
              let new_auth = await response2.json();
              this.loginInfo.auth_token = new_auth.auth_token;
              localStorage.setItem("user-data", JSON.stringify(this.loginInfo));
              return await this.requestWithAuth(path, data);
            } else {
              this.logOut();
              return {
                status: 5,
                error: await response.text()
              };
            }
          });
        } else {
          return {
            status: 4,
            error: await response.text()
          };
        }
      });
    } else {
      return {
        status: 5,
        error: "Unauthorized request!"
      };
    }
  }
}
var apiInstance,
  get = () => {
    if (apiInstance) return apiInstance;
    else {
      apiInstance = new API();
      return apiInstance;
    }
  };

//subhasis@cyanberg.com

export default get();
