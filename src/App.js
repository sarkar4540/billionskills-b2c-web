import React, { useState } from "react";
import {
  Grid,
  Tabs,
  Tab,
  AppBar,
  Hidden,
  IconButton,
  Drawer,
  Toolbar
} from "@material-ui/core";
import { withRouter, Link, Switch, Route } from "react-router-dom";
import { Apps, AccountCircle, Menu } from "@material-ui/icons";
import "./App.css";
import {
  ProfileCompact,
  Profile,
  ComponentSwitch,
  NavTabs,
  Banners,
  Account,
  Portfolio,
  Jobs,
  Connects,
  Posts,
  Enrolls,
  UserDetails
} from "./components";

const allTabs = [
  { path: "/profile", name: "Profile", view: true, component: <Profile /> },
  {
    path: "/portfolio",
    name: "Portfolio",
    view: true,
    component: <Portfolio />
  },
  { path: "/jobs", name: "Jobs", view: true, component: <Jobs /> },
  { path: "/connects", name: "Connects", view: true, component: <Connects /> },
  { path: "/posts", name: "Articles", view: true, component: <Posts self /> },
  { path: "/enroll", name: "Enroll", view: true, component: <Enrolls /> },
  { path: "/other_apps", name: "Other Apps", view: true, icon: <Apps /> },
  { path: "/user_details", name: "User Details", component: <UserDetails /> },
  {
    path: "/account",
    name: "Account",
    view: true,
    icon: <AccountCircle />,
    component: <Account />
  }
];

const App = withRouter(props => {
  const [openDrawer, setOpenDrawer] = useState(false);
  return (
    <div className="App">
      <AppBar elevation={5} position="fixed">
        <Toolbar>
          <Hidden smDown>
            <Grid container>
              <Grid item md={2}>
                <Link to="/">
                  <img src="/logo.png" style={{ width: "50px" }} />
                </Link>
              </Grid>
              <Grid item md={10}>
                <NavTabs tabs={allTabs} value={props.location.pathname} />
              </Grid>
            </Grid>
          </Hidden>

          <Hidden mdUp>
            <Grid container>
              <Grid item xs={2}>
                <IconButton
                  onClick={() => {
                    setOpenDrawer(true);
                  }}
                >
                  <Menu />
                </IconButton>
                <Drawer
                  open={openDrawer}
                  onClose={() => {
                    setOpenDrawer(false);
                  }}
                  anchor="left"
                >
                  <NavTabs
                    tabs={allTabs}
                    value={props.location.pathname}
                    callback={() => {
                      setOpenDrawer(false);
                    }}
                    vertical
                  />
                </Drawer>
              </Grid>
              <Grid item xs={8}>
                <Link to="/">
                  <img src="/logo.png" style={{ width: "50px" }} />
                </Link>
              </Grid>
            </Grid>
          </Hidden>
        </Toolbar>
      </AppBar>
      <Toolbar style={{ marginBottom: "15px" }} />
      <Grid
        container
        spacing={2}
        style={{ overflowX: "hidden", width: "100%", padding: "15px" }}
      >
        {(props.location.pathname === ('/') || props.location.pathname.startsWith('/profile') || props.location.pathname.startsWith('/portfolio')) ?
          <React.Fragment>
            <Hidden smDown>
              <Grid item md={3}>
                <ProfileCompact />
              </Grid>
            </Hidden>
            <Grid item md={6} xs={12}>
              <ComponentSwitch location={props.location} tabs={allTabs} home />
            </Grid>
          </React.Fragment> :
          <Grid item md={9} xs={12}>
            <ComponentSwitch location={props.location} tabs={allTabs} home />
          </Grid>}
        <Hidden smDown>
          <Grid item md={3} xs={12}>
            <Banners style={{ width: "100%" }} />
          </Grid>
        </Hidden>
      </Grid>
    </div>
  );
});

export { App };
